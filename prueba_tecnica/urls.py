"""prueba_tecnica URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView
from rest_framework.schemas import get_schema_view

from users import views
#from usuarios import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('productos', include('productos.urls')),
    url(r'pedidos/', include('pedidos.urls')),
    path('', views.pedido),
    path('register', views.register),
    path('login', views.login),
    path('logout', views.logout),
    path('accounts/', include('django.contrib.auth.urls')), # new
    path('openapi/', get_schema_view(
        title="Zapatos Bernini",
        description="Utilización de la API"
    ), name='openapi-schema'),
    path('docs/', TemplateView.as_view(
        template_name='documentation.html',
        extra_context={'schema_url':'openapi-schema'}
    ), name='swagger-ui'),

]
