from django.conf.urls import url

from pedidos import views

urlpatterns = [
    url(r'^', views.pedido, name='pedido'),
]