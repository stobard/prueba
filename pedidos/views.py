from django.contrib.auth.decorators import login_required

# Create your views here.
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser

from productos.serializers import ProductoSerializer
from productos.views import export_order_to_xlsx, JSONResponse, sendEmail


@csrf_exempt
@login_required
def pedido(request):
    if request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = ProductoSerializer(data=data, many=True)
        if serializer.is_valid():
            export_order_to_xlsx(serializer.data, request.user)
            sendEmail()
            return JSONResponse(serializer.data, status=201)
        return JSONResponse(serializer.errors, status=400)