from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import Usuario


class CustomUserCreationForm(UserCreationForm):
    username = None
    email = forms.EmailField(label='Enter email')
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        model = Usuario
        fields = ("email",)