import profile

from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.shortcuts import render
from django.contrib.auth import logout as do_logout, authenticate
from django.contrib.auth import login as do_login

# Create your views here.
from django.shortcuts import render, redirect

from productos.models import Producto

from django.contrib.auth import get_user_model

from users.forms import CustomUserCreationForm

#User = get_user_model()
from users.models import Usuario


def pedido(request):
    if request.user.is_authenticated:
        productos = Producto.objects.all()
        return render(request, "users/pedidos.html", {'productos': productos})
    return redirect('/login')


def register(request):
    form = CustomUserCreationForm()
    if request.method == "POST":
        form = CustomUserCreationForm(data=request.POST)
        if form.is_valid():
            user = form.save()
            if user is not None:
                do_login(request, user)
                return redirect('/')

    form.fields['password1'].help_text = None
    form.fields['password2'].help_text = None

    return render(request, "users/register.html", {'form': form})


def login(request):
    form = AuthenticationForm()
    if request.method == "POST":
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            email = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = authenticate(email=email, password=password)

            if user is not None:
                do_login(request, user)
                return redirect('/')

    return render(request, "users/login.html", {'form': form})


def logout(request):
    do_logout(request)
    return redirect('/')