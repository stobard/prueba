import datetime
import json
import time

import requests
from django.http import HttpResponse
from mailjet_rest import Client
from django.conf import settings
import xlsxwriter
import base64

# Create your views here.
from django.views.decorators.csrf import csrf_exempt
from rest_framework import generics, schemas
from rest_framework.parsers import JSONParser
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response

from productos.models import Producto
from productos.serializers import ProductoSerializer


class ProductList(generics.ListCreateAPIView):
    queryset = Producto.objects.all()
    serializer_class = ProductoSerializer
    http_method_names = ['get']


class JSONResponse(HttpResponse):
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


def sendEmail():
    API_KEY = getattr(settings, "API_KEY", None)
    API_SECRET = getattr(settings, "API_SECRET", None)
    EMAIL_TIENDA = getattr(settings, "EMAIL_TIENDA", None)
    encoded = base64.b64encode(open("productos.xlsx","rb").read()).decode('UTF-8')
    mailjet = Client(auth=(API_KEY, API_SECRET), version='v3.1')
    data = {
        'Messages': [
            {
                "From": {
                    "Email": "roberto.monteagudo.sanz@gmail.com",
                    "Name": "Zapatos Bernini"
                },
                "To": [
                    {
                        "Email": EMAIL_TIENDA,
                        "Name": "Administrador Tienda Zapatos Bernini"
                    }
                ],
                "Subject": "Han realizado un pedido",
                "TextPart": "Pedido realizado correctamente",
                "HTMLPart": "Hemos recibido un pedido",
                "CustomID": "HDo%imP*e1iR",
                "InlinedAttachments": [
                    {
                            "ContentType": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                            "Filename": "productos.xlsx",
                            "ContentID": "id1",
                            "Base64Content": str(encoded)
                    }
                ]
            }
        ]
    }

    result = mailjet.send.create(data=data)


def export_order_to_xlsx(data, email):
    workbook = xlsxwriter.Workbook('productos.xlsx')
    worksheet = workbook.add_worksheet()

    productos =data

    row = 0
    precio_total = 0

    worksheet.write(row, 0, 'Usuario')
    worksheet.write(row, 1, str(email))
    row +=1

    x = datetime.datetime.now()
    fecha_pedido = "%s/%s/%s" % (x.day, x.month, x.year)
    horario_pedido = time.strftime("%X")
    worksheet.write(row, 0, 'Fecha pedido')
    worksheet.write(row, 1, fecha_pedido)
    row += 1
    worksheet.write(row, 0, 'Hora pedido')
    worksheet.write(row, 1, horario_pedido)
    row += 2

    worksheet.write(row, 0, 'Producto')
    worksheet.write(row, 1, 'Cantidad')
    worksheet.write(row, 2, 'Precio unitario')
    row += 1
    for product in productos:
        col = 0
        cantidad = 0
        for key, value in product.items():
            worksheet.write(row, col, value)
            if key == "cantidad":
                cantidad = value
            if key == "precio":
                precio_total += value * cantidad
            col += 1
        row +=1

    row += 1

    worksheet.write(row, 0, 'Total en euros')
    worksheet.write(row, 1, precio_total)
    row += 1

    worksheet.write(row, 0, 'Total en dolares')
    dolares = cambio_euro_dolar(precio_total)
    worksheet.write(row, 1, dolares)
    workbook.close()


def cambio_euro_dolar(cantidad_total):
    response = requests.get('https://api.exchangeratesapi.io/latest?symbols=USD')
    respuesta = response.json()
    precio_dolar_actual = respuesta['rates']['USD']
    precio_dolar = cantidad_total * precio_dolar_actual
    return precio_dolar


def schema_view(request):
    generator = schemas.SchemaGenerator(title='Rest Swagger')
    return Response(generator.get_schema(request=request))