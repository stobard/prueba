from sys import path

from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework_swagger.views import get_swagger_view

from productos import views
schema_view = get_swagger_view(title='Catalogo de productos')

urlpatterns = [
    url('', views.ProductList.as_view()),
    url(r'$', schema_view)
]