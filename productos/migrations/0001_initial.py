from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Producto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=200, verbose_name='Nombre del producto')),
                ('cantidad', models.IntegerField(default=1, verbose_name='Cantidad del producto')),
                ('precio', models.FloatField(verbose_name='Precio del producto')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Última actualización')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Creación')),
            ],
        ),
    ]
