from time import timezone
from django.utils.translation import ugettext_lazy as _

from django.db import models
# Create your models here.


class Producto(models.Model):
    """
    Model for the product
    """
    nombre = models.CharField(verbose_name=_(u'Nombre del producto'),max_length=200)
    cantidad = models.IntegerField(verbose_name=_(u'Cantidad del producto'), default=1, blank=False)
    precio = models.FloatField(verbose_name=_(u'Precio del producto'), null=False, blank=False)
    updated_at = models.DateTimeField(verbose_name=_(u'Última actualización'), auto_now=True)
    created_at = models.DateTimeField(verbose_name=_(u'Creación'), auto_now_add=True)

    def __str__(self):
        return self.nombre

